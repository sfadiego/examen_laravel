<?php

namespace Examen\Models;

use Illuminate\Database\Eloquent\Model;

class ServicesModel extends Model
{
    protected $table = 'services';
    protected $fillable = [
        'user_id', 'name', 'status'
    ];
}
