{{-- listadousuarios --}}
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
         
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">-</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($data)>0)    
                    @foreach ($data as $item)
                    <tr>
                      <th scope="row">{{ $item['id'] }}</th>
                      <td>{{ $item['nombre'] }}</td>
                      <td>{{ $item['status'] === 0 ? "Inactivo" : "Activo" }}</td>
                      <td>
                          <a class="btn btn-primary btn-sm" href="{{ URL::to('detall-usuario').'/'.$item['id'] }}">Detalle</a>
                      </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <th scope="row">Sin información</th>
                      <th scope="row">Sin información</th>
                      <th scope="row">Sin información</th>
                    </tr>
                  @endif
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    function borrar(id) {
        
        fetch(`${site_url}/services/delete/${id}`,{
            method: 'DELETE',
        })
        .then(response => response.json())
        .then(response=>{
            if(!response.ok){
              alert("Ha ocurrido un error");
            }
            return window.location.href = `${site_url}/listado`
        });

    };

</script>
@endsection