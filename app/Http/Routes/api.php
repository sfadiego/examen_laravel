<?php
use Illuminate\Http\Request;

Route::group(['prefix' => 'services'], function () {
    Route::get('/', 'ServicesController@get');
    Route::post('store', 'ServicesController@store');
    Route::put('update/{id}', 'ServicesController@update');
    Route::delete('delete/{id}', 'ServicesController@delete');
    
});

Route::group(['prefix' => 'usuarios'], function () {
    Route::get('/', 'UsuariosController@get');
    Route::post('store', 'UsuariosController@store');
    Route::put('update/{id}', 'UsuariosController@update');
    Route::get('detalle/{id}', 'UsuariosController@detalle');
    
    Route::get('usuario-servicio', 'UsuariosController@listado');
});
