<?php

namespace Examen\Http\Controllers\Auth;

use Examen\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'correo' => 'required|email',
            'password' => 'required',
        ]);
        
        if ($validatedData->fails()) {
            return view('auth.login')->withErrors($validatedData);
        }

        $credentials = $request->only('correo', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/home');
        }else{
            return view('auth.login')->with('mensaje',"Error en las credenciales");
        }
    }
}
