<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::post('/login', 'Auth\LoginController@authenticate');
Route::get('/login', function () {
    return view('auth.login');
});

Route::group(['middleware' => 'userstatus'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    Route::get('/listado', 'HomeController@listado');
    Route::get('/listado-usuarios', 'HomeController@listadoUsuarios');
    Route::get('/servicio-editar/{id}', 'HomeController@servicioeditar');
    Route::get('/servicio-crear', 'HomeController@serviciocrear');
    Route::get('/detall-usuario/{id}', 'HomeController@detalleUsuario');
});
