@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form id="form-update-service">
                <div class="form-group">
                  <label for="nombreServicio">Servicio</label>
                    <input type="text" name="name" class="form-control" id="nombreServicio" value="{{$data['name']}}" placeholder="Nombre de servicio">
                    <input type="hidden" id="id" name="id" value="{{$data['id']}}">
                    <span id="name-error" class="help-block">
                    </span>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Usuario</label>
                    <select name="user_id" id="user_id" class="form-control">
                        <option value="">Seleccionar</option>
                        @foreach ($usuarios as $item)
                            <option {{ $data['user_id'] === $item['id'] ? "selected": ""}} value="{{$item['id']}}">{{$item['nombre']}}</option>
                        @endforeach
                    </select>
                    <span id="user_id-error" class="help-block"></span>
                  </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label" for="inlineCheckbox1">Status</label>
                    <input class="form-check-input" name="status" type="checkbox" id="status" <?php echo $data['status'] === 1 ? 'checked' :'' ?> value="{{$data['status']}}">
                    <span id="status-error" class="help-block"></span>
                  </div>
                  <br>
                  <div class="form-group">
                      <button class="btn btn-primary" onclick="actualizar({{$data['id']}})" type="button">
                          Actualizar 
                      </button>
                  </div>
              </form>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    function actualizar(id) {
        let elementCheck = document.getElementById('status');
        let checked = elementCheck.checked === true ? 1 : 0;
        fetch(`${site_url}/services/update/${id}`,{
            method: 'PUT',
            body:JSON.stringify({
                name:document.getElementById('nombreServicio').value,
                status:checked
            }),
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(response=>{
            if(response.errors){
               return printErrors(response);
            }
            return window.location.href = `${site_url}/listado`
        });

    };

    function printErrors(obj) {
        let dataerror = Object.keys(obj.errors);
        dataerror.map(item =>{
            let err_msg = obj.errors[item][0];
            let name_item = `${item}-error`;
            document.getElementById(name_item).innerHTML = '<strong>'+err_msg +'</strong>';
        });
    }
</script>
@endsection