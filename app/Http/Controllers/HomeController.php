<?php

namespace Examen\Http\Controllers;

use Examen\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Examen\Models\ServicesModel as ServicesModel;
use Examen\Models\User as UserModel;

class HomeController extends Controller
{
    private $modeloServicio;
    private $modeloUsuarios;
    public function __construct()
    {
        $this->middleware('auth');
        $this->modeloServicio = new ServicesModel();
        $this->modeloUsuarios = new UserModel();
    }

    public function index()
    {
        return view('home');
    }

    public function listado()
    {
        $data = $this->modeloServicio->get()->toArray();
        return view('servicio.listado')->with('data', $data);
    }

    public function servicioeditar($id)
    {
        $data = $this->modeloServicio->find($id)->toArray();
        $usuarios = $this->modeloUsuarios->get()->toArray();
        return view('servicio.editar',[
            'data'=> $data,
            'usuarios'=> $usuarios,
        ]);
    }

    public function serviciocrear()
    {
        $data = $this->modeloUsuarios->get()->toArray();
        return view('servicio.crear',[
            'usuarios'=> $data,
        ]);
    }

    public function listadoUsuarios()
    {
        $data = $this->modeloUsuarios->where('rol_id', UserModel::ROL_USER)->get()->toArray();
        return view('usuarios.listado')->with('data', $data);
    }

    public function detalleUsuario($id)
    {
        $data = $this->modeloUsuarios->find($id);
        $data['servicios'] = $data->servicios;
        return view('usuarios.detalle',[ 'data'=> $data->toArray() ]);
    }
}
