<?php

namespace Examen\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = [
        'nombre',
        'correo',
        'password',
        'rol_id',
        'age',
        'gender',
        'status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function servicios()
    {
        return $this->hasMany('Examen\Models\ServicesModel');
    }
    
    const  ROL_USER = 2;
    const  ROL_ADMIN = 1;
}
