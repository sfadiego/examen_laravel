@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-9">
          <a class="btn btn-success" href="{{ URL::to('servicio-crear')}}">Crear servicio</a>
        </div>
        <div class="col-md-10 col-md-offset-1">

            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Servicio</th>
                    <th scope="col">-</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                    <tr>
                      <th scope="row">{{ $item['id'] }}</th>
                      <td>{{ $item['name'] }}</td>
                      <td>
                          <a class="btn btn-primary btn-sm" href="{{ URL::to('servicio-editar').'/'.$item['id'] }}">Editar</a>
                      </td>
                      <td>
                        <button class="btn btn-danger btn-sm" onclick="borrar({{$item['id']}})">Borrar</button>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    function borrar(id) {
        
        fetch(`${site_url}/services/delete/${id}`,{
            method: 'DELETE',
        })
        .then(response => response.json())
        .then(response=>{
            if(!response.ok){
              alert("Ha ocurrido un error");
            }
            return window.location.href = `${site_url}/listado`
        });

    };

</script>
@endsection