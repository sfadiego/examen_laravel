@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form id="form-update-service">
                <div class="form-group">
                  <label for="nombreServicio">Nombre</label>
                    <input type="text" name="nombre" readonly class="form-control" id="nombre" value="{{$data['nombre']}}" placeholder="Nombre de usuario">
                    <input type="hidden" id="id" name="id" value="{{$data['id']}}">
                    <span id="nombre-error" class="help-block">
                    </span>
                </div>
                <div class="form-group">
                    <label for="servicios">Servicios</label>
                    <ul class="list-group">
                        @foreach ($data['servicios'] as $item)
                            <li class="list-group-item">{{ $item['name'] }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label" for="inlineCheckbox1">Activo</label>
                    <input class="form-check-input" name="status" type="checkbox" id="status" <?php echo $data['status'] === 1 ? 'checked' :'' ?> value="{{$data['status']}}">
                    <span id="status-error" class="help-block"></span>
                  </div>
                  <br>
                  <div class="form-group">
                      <button class="btn btn-primary" onclick="actualizar({{$data['id']}})" type="button">
                          Actualizar 
                      </button>
                  </div>
              </form>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    function actualizar(id) {
        let elementCheck = document.getElementById('status');
        let checked = elementCheck.checked === true ? 1 : 0;
        fetch(`${site_url}/usuarios/update/${id}`,{
            method: 'PUT',
            body:JSON.stringify({
                status:checked
            }),
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(response=>{
            if(response.errors){
               return printErrors(response);
            }
            return window.location.href = `${site_url}/listado-usuarios`
        });

    };

    function printErrors(obj) {
        let dataerror = Object.keys(obj.errors);
        dataerror.map(item =>{
            let err_msg = obj.errors[item][0];
            let name_item = `${item}-error`;
            document.getElementById(name_item).innerHTML = '<strong>'+err_msg +'</strong>';
        });
    }
</script>
@endsection