<?php

namespace Examen\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Examen\Http\Requests;
use Examen\Models\User as UserModel;
use Examen\Models\ServicesModel as ServicesModel;

class UsuariosController extends Controller
{
    private $modelo;
    public function __construct()
    {
        $this->modelo = new UserModel();
        $this->serviceModel = new ServicesModel();
    }

    public function store(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'nombre' => 'required',
            'correo' => 'required|unique:users,correo|email',
            'password' => 'required',
            'age' => 'required|numeric',
            'gender' => 'required',
            'status' => 'required|numeric',
        ]);

        if ($rules->fails()) {
            return ['errors' => $rules->errors()];
        }

        $this->modelo->nombre = $request->nombre;
        $this->modelo->correo = $request->correo;
        $this->modelo->password = $request->password;
        $this->modelo->age = $request->age;
        $this->modelo->gender = $request->gender;
        $this->modelo->status = $request->status;
        $this->modelo->save();
        return $this->modelo;
    }

    public function get()
    {
        return $this->modelo->all();
    }

    public function detalle($id)
    {
        $data = $this->modelo->find($id);
        $data['servicios'] = $data->servicios;
        return $data;
    }

    public function update(Request $request, $id)
    {
        $rules = Validator::make($request->all(), [
            'status' => 'required|numeric',
        ]);

        if ($rules->fails()) {
            return ['errors' => $rules->errors()];
        }
        $data = $this->modelo->find($id);
        $data->status = $request->status;
        $data->save();
        return $data;
    }

    public function listado()
    {
        $all = $this->modelo->get();
        if (count($all) > 0) {
            foreach ($all as $key => $item) {
                $all[$key]['services'] = $this->serviceModel->where('user_id', $item['id'])->get();
            }
            return [
                'status' => 'OK',
                'data' => $all,
            ];
        }

        return [
            'status' => 'False',
            'data' => []
        ];
    }
}
