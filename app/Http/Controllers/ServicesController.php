<?php

namespace Examen\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Examen\Http\Requests;
use Examen\Models\ServicesModel as ServicesModel;
use Examen\Models\User as UserModel;

class ServicesController extends Controller
{
    private $modelo;
    public function __construct()
    {
        $this->modelo = new ServicesModel();
        $this->modeloUsuarios = new UserModel();
    }

    public function store(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'name' => 'required',
            'status' => 'required|numeric',
        ]);

        if ($rules->fails()) {
            return ['errors' => $rules->errors()];
        }

        $this->modelo->user_id = $request->user_id;
        $this->modelo->name = $request->name;
        $this->modelo->status = $request->status;
        $this->modelo->save();
        return $this->modelo;
    }

    public function update(Request $request, $id)
    {
        $rules = Validator::make($request->all(), [
            'status' => 'required|numeric',
            'name' => 'required',
        ]);

        if ($rules->fails()) {
            return ['errors' => $rules->errors()];
        }
        $data = $this->modelo->find($id);
        $data->name = $request->name;
        $data->status = $request->status;
        $data->save();
        return $data;
    }

    public function delete($id)
    {
        $data = $this->modelo->find($id);
        $data->delete();
        return ['ok' => true];
    }

    public function get()
    {
        return $this->modelo->all();
    }

    public function getById($id = null)
    {
        if($id === null){
            return [];
        }
        
        return $this->modelo->find($id)->toArray();
    }

}
