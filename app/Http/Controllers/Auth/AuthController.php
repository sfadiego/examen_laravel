<?php

namespace Examen\Http\Controllers\Auth;

use Examen\Models\User;
use Validator;
use Examen\Http\Controllers\Controller;
use Examen\Http\Requests\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    public function __construct()
    {

        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    protected function validator(array $data)
    {
        
        return  Validator::make($data, [
            'nombre' => 'required|max:255',
            'correo' => 'required|email|unique:users,correo',
            'password' => 'required|min:6',
            'age' => 'required|numeric',
            'gender' => 'required',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'nombre' => $data['nombre'],
            'correo' => $data['correo'],
            'password' => bcrypt($data['password']),
            'age' => $data['age'],
            'gender' => $data['gender'],
        ]);
    }

}
