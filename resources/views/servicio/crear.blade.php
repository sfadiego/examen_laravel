@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form id="form-update-service">
                <div class="form-group">
                  <label for="nombreServicio">Servicio</label>
                    <input type="text" name="name" class="form-control" id="nombreServicio" placeholder="Nombre de servicio">
                    <span id="name-error" class="help-block"></span>
                </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Usuario</label>
                    <select name="user_id" id="user_id" class="form-control">
                        <option value="">Seleccionar</option>
                        @foreach ($usuarios as $item)
                            <option value="{{$item['id']}}">{{$item['nombre']}}</option>
                        @endforeach
                    </select>
                    <span id="user_id-error" class="help-block"></span>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label" for="inlineCheckbox1">Status</label>
                    <input class="form-check-input" name="status" type="checkbox" id="status" >
                    <span id="status-error" class="help-block"></span>
                  </div>
                  <br>
                  <div class="form-group">
                      <button class="btn btn-primary" onclick="crear()" type="button">
                          Crear 
                      </button>
                  </div>
              </form>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    function crear() {
        let elementCheck = document.getElementById('status');
        let checked = elementCheck.checked === true ? 1 : 0;
        fetch(`${site_url}/services/store`,{
            method: 'POST',
            body:JSON.stringify({
                name:document.getElementById('nombreServicio').value,
                status:checked,
                user_id: document.getElementById("user_id").value
            }),
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(response=>{
            if(response.errors){
               return printErrors(response);
            }
            return window.location.href = `${site_url}/listado`
        });

    };

    function printErrors(obj) {
        let dataerror = Object.keys(obj.errors);
        dataerror.map(item =>{
            let err_msg = obj.errors[item][0];
            let name_item = `${item}-error`;
            document.getElementById(name_item).innerHTML = '<strong>'+err_msg +'</strong>';
        });
    }
</script>
@endsection